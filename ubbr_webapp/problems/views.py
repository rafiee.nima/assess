from django.shortcuts import render, HttpResponseRedirect, reverse
from django.template import Template
from django.http import HttpResponse
from django.conf import settings

from django.template import RequestContext
import datetime
from django.utils.timezone import utc
from json import dumps
from django.core.serializers.json import DjangoJSONEncoder
from django.core.serializers import serialize

# adding the ubbr namespace to the path
# assuming that is two levels above the django project root
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(settings.BASE_DIR)))
from django.forms.models import model_to_dict
from ubbr.engine.core import Ubbr
from ubbr.engine.graders.graders import StringInputGrader, IntegerInputGrader,grader_from_data_type
from django.contrib.auth.decorators import login_required
from students.models import StudentGrade
from models import Question, Sheet

import json

# Create your views here.

@login_required
def problem_view(request, q_id, sheet_id):
    question = Question.objects.get(id=q_id)
    ubbr = Ubbr(question.source)
    # get the template string and context values from the Ubbr
    template_string = ubbr.template
    random_seed = request.GET.get('seed', '1')

    ubbrvalues = ubbr.get_context(random_seed=random_seed)[0]

    context = RequestContext(request, autoescape=False) # we use a RequestContext to enable csrf protection
    context.update({
        'ubbrvalues': ubbrvalues,
        'problem_id': question.id,
        'sheet_id': sheet_id,
        })
    template = Template(template_string)
    #return HttpResponse(ubbrvalues)
    return HttpResponse(template.render(context))

def get_template(q_source):
    ubbr = Ubbr(q_source)
    template = ubbr.template
    ubbrvalues = ubbr.get_context(random_seed=1)
    return {'ubbrvalues': ubbrvalues, 'template': template}


@login_required
def problem_grade(request, q_id, sheet_id):
    if request.method != 'POST':
        return HttpResponse('Only POST requests are supported')
    # load the Ubbr from the DB and get the answer data
    question = Question.objects.get(id=q_id)
    ubbr = Ubbr(question.source)
    # get the template and context values from the Ubbr
    template_string = ubbr.template
    random_seed = request.GET.get('seed', '1')
    ubbrdata = ubbr.get_context(random_seed=random_seed)[1]

    # now use the graders to compare the submitted values with the
    # correct answers
    results = {}
    for inp in ubbrdata:
        grader = grader_from_data_type(inp['data_type'])().grade
        
        submitted = request.POST.get('ubbr-input-{}'.format(inp['data_id']), None)
        results.update({
            inp['data_id']: grader(submitted, inp)
            })
    if request.user.is_authenticated():
        user = request.user
    dct = json.loads(json.dumps(results))['0']
    grade = dct['score']

    q = Question.objects.get(id=q_id)
    sheet = Sheet.objects.get(id=sheet_id)
    std_grd, created = StudentGrade.objects.get_or_create(user=user, question=q, sheet=sheet, )
    std_grd.grade = grade
    std_grd.save()

    #context = {'grade':grade, 'question':q, 'sheet': sheet}
    return HttpResponseRedirect(reverse('SheetEdit', args=[sheet_id ]))

@login_required
def load_sheets(request):
    data_dict = {}
    status = " "
    current_date = datetime.datetime.utcnow().replace(tzinfo=utc)
    try:
        sheets = Sheet.objects.filter(end_date__gt=current_date)

        status = "Success"
    except:
        status = "ObjectNotExist"
        sheets = []
    # for sheet in sheets:
    #     data_dict[sheet.id] = model_to_dict(sheet)
    context = {'sheets': sheets, 'status': status}
#    data = dumps(raw_data, cls=DjangoJSONEncoder)
    return render(request, 'sheetlist.html', context)


@login_required
def sheet_edit(request, id):

    sheet = Sheet.objects.get(id=id)
    current_date = datetime.datetime.utcnow().replace(tzinfo=utc)
    if sheet.end_date < current_date.date():
        return HttpResponseRedirect(reverse('LoadSheets'))
    if request.user.is_authenticated():
        user = request.user

    stdgrad_lst = StudentGrade.objects.filter(sheet=sheet, user=user)
    context = {'data': stdgrad_lst}
    return render(request, 'sheetdetail.html', context)

