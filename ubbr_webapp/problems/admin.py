from django.contrib import admin

# Register your models here.

from problems.models import Question, Sheet


class ModelQuestionAdmin(admin.ModelAdmin):
    class Meta:
        model = Question
    list_display = ('__str__', 'description',)


class ModelSheetAdmin(admin.ModelAdmin):
    class Meta:
        model = Sheet
    list_display = ('type', 'start_date', 'end_date', 'active')


admin.site.register(Question, ModelQuestionAdmin)
admin.site.register(Sheet, ModelSheetAdmin)


