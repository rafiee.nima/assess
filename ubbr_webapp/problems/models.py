from django.db import models
# Create your models here.

from django.urls import reverse

class Question(models.Model):
    source = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    title = models.CharField(max_length=100, verbose_name="Title")
    type = models.CharField(max_length=50, verbose_name="Question Type")

    def __str__(self):
        return str(self.title)

    def get_absolute_url(self):
        return reverse('problem', args=[self.id])

class Sheet(models.Model):
    type = models.CharField(max_length=50, verbose_name='type')
    title = models.CharField(max_length=200 , verbose_name='Title')
    q1 = models.ForeignKey(Question, related_name='q1')
    q2 = models.ForeignKey(Question, related_name='q2')
    q3 = models.ForeignKey(Question, related_name='q3')
    q4 = models.ForeignKey(Question, related_name='q4')
    q5 = models.ForeignKey(Question, related_name='q5')
    q6 = models.ForeignKey(Question, related_name='q6')
    q7 = models.ForeignKey(Question, related_name='q7')
    q8 = models.ForeignKey(Question, related_name='q8')
    q9 = models.ForeignKey(Question, related_name='q9')
    q10 = models.ForeignKey(Question, related_name='q10')
    active = models.BooleanField(default=False, verbose_name="Active")
    num_of_questions = models.IntegerField(verbose_name="Number of Questions")
    start_date = models.DateField(verbose_name="Start Date")
    end_date = models.DateField(verbose_name="End Date")


    def __str__(self):
        return str(self.title)