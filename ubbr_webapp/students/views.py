# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import auth
from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
# Create your views here.

def login(request):


    if request.method != "POST":
        return render(request, 'index.html')

    username = request.POST['username']
    password = request.POST['password']

    user = auth.authenticate(username=username, password=password)
    if user:
        if user.last_login:
            if user.is_active:
                auth.login(request, user)

                return HttpResponseRedirect(reverse('LoadSheets',))
                # here need to redirect to a function which load sheets
            else:
                return HttpResponse("User is not active")
        else:

            return render(request, 'passchange.html')


    else:
        return HttpResponse("User Does Not Exist")

@login_required
def pass_change(request):

    if request.method != "POST":
        return render(request, 'index.html')

    pass1 = request.POST['password1']
    pass2 = request.POST['password2']
    if pass1 != pass2:
        return HttpResponse("passwords does not match")


    user = request.user

    user.set_password(pass1)
    user.save()

    return HttpResponseRedirect(reverse('LoadSheets',))
