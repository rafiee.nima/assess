# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.

from models import Student, StudentGrade, StudentList



class StudentGradeAdmin(admin.ModelAdmin):
    class Meta:
        model = StudentGrade
    list_display = ('__str__', 'grade', 'question', 'sheet')


class StudentAdmin(admin.ModelAdmin):
    class Meta:
        model = Student
    list_display = ('__str__',)

class StudenListAdmin(admin.ModelAdmin):
    class Meta:
        model = StudentList



admin.site.register(StudentGrade, StudentGradeAdmin)
admin.site.register(Student, StudentAdmin)
admin.site.register(StudentList, StudenListAdmin)