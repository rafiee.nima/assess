# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

"""
A ForeignKey is for one-to-many, so a Car object might have many Wheels, each Wheel having a
 ForeignKey to the Car it belongs to. A OneToOneField would be like an Engine, where a Car
  object can have one and only one.
"""


# Create your models here.
from django.db import models
from django.contrib.auth.models import User
from problems.models import Sheet, Question


class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    grade = models.IntegerField(default=0)
    last_login_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.user.username

class StudentGrade(models.Model):
    user = models.ForeignKey(User, verbose_name='Student')
    question = models.ForeignKey(Question , verbose_name='question')
    sheet = models.ForeignKey(Sheet , verbose_name='sheet')
    grade = models.FloatField( verbose_name="grade", null=True, )

    def __str__(self):
        return self.user.username

class StudentList(models.Model):
    file = models.FileField(upload_to='uploads')