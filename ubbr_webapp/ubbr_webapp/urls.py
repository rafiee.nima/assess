"""ubbr_webapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from problems.views import problem_view, problem_grade, sheet_edit, load_sheets
from students.views import login, pass_change

from views import index
urlpatterns = [
    url(r'^$', index , name="Index"),
    url(r'^admin/', admin.site.urls),
    url(r'^problem/(\d+)/(\d+)/$', problem_view, name='problem'),
    url(r'^grade/(\d+)/(\d+)/$', problem_grade, name='Grade' ),
    url(r'^edit/(\d+)/$', sheet_edit, name='SheetEdit'),
    url(r'^login/$', login, name='StudentLogin'),
    url(r'^pass_change/$', pass_change, name='PassChange'),
    url(r'^load_sheet/$', load_sheets, name='LoadSheets')

]
